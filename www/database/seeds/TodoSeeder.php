<?php

use Illuminate\Database\Seeder;

class TodoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todo')->insert([
           [ 'id' => 1,
             'title' => 'TO DO'
           ],
           [
             'id' => 2,
             'title' => 'DONE'
           ]
        ]);
    }
}
