<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class todo extends Model
{
    protected $fillable = ['title'];

    public function tasks()
    {
        return $this->hasMany('App\tasks')->orderBy('created_at', 'DESC');
    }
}
