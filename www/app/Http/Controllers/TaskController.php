<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tasks;

class TaskController extends Controller
{
    public function index()
    {
        return todo::all();
    }

    public function show(Request $request, $id)
    {
        $task = tasks::findOrFail($id);
        return response()->json($task, 201);
    }

    public function store(Request $request)
    {
        $task = tasks::create($request->all());
        return response()->json(["created" => true],201);
    }

    public function update(Request $request, tasks $task)
    {
        $todo->update($request->all());

        return response()->json($todo, 200);
    }

    public function delete(tasks $task)
    {
        $task->delete();

        return response()->json(null, 204);
    }
}
