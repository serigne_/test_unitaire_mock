<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\todo;

class TodoController extends Controller
{
    public function index()
    {
        return todo::all();
    }

    public function show(Request $request, $id)
    {
        $todo = todo::findOrFail($id);
        return response()->json($todo, 201);
    }

    public function store(Request $request)
    {
        $todo = todo::create($request->all());
        return response()->json(["created" => true],201);
    }

    public function update(Request $request, todo $todo)
    {
        $todo->update($request->all());

        return response()->json($todo, 200);
    }

    public function delete(todo $todo)
    {
        $todo->delete();

        return response()->json(null, 204);
    }
}
