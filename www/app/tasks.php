<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tasks extends Model
{
    protected $fillable = ['title', 'description'];

    public function todo()
    {
        return $this->belongsTo('App\todo');
    }
}
