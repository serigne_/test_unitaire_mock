<?php

namespace Tests\Unit;

//use Illuminate\Foundation\Testing\TestCase;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
//use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use App\todo;
use App\tasks;

class TodoListTest extends TestCase
{

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateTodo()
    {
        $response = $this->json('POST', '/todo', [
            'title' => 'test'
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);

    }

    public function testCreateTask()
    {
        $response = $this->json('POST', '/task', [
            'title' => 'task 1',
            'description' => 'Tache à faire'
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }

    public function testAddTask()
    {
        $task = tasks::find(1);
        $todo = todo::find(1);

        $task->todo()->associate($todo);

        $task->save();

        $response = $this->json('GET','/todo/1');
        $this->assertEquals(1, todo::find(1)->tasks()->count());
    }

    public function testRemoveTaskFromTodo()
    {
        $task = tasks::find(1);
        $todo = todo::find(1);

        $this->assertEquals(1, todo::find(1)->tasks()->count());

        $task->todo()->dissociate($todo);

        $task->save();

        $this->assertEquals(0, todo::find(1)->tasks()->count());

    }
}
